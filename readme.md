#LIBRARY APP DEMO

##Installation

+ install php, compose, mysql, nodejs
+ cd project root directory
+ create database in mysql server and then set appropriate mysql configuration connection in .env (local) or in config/database.php
+ run `composer install`
+ run `php artisan migrate --seed`
+ run `npm install`
+ run `npm run build`

##Config http server
+ Add new site in nginx (apache) then config root to project/root/dir/public

##Default users
+ 'username' => 'GreatAdmin','email' => 'admin@abc.com','password' => 'admin',
+ 'username' => 'GreatRedactor','email' => 'libr@abc.com', 'password' => 'redac',
+ 'username' => 'Walker', 'email' => 'walker@gmail.com', 'password' => 'walker',
