import * as constants from '../utils/constants';

export const selectedCategory = (state = constants.ALL_CATEGORIES, action) => {
  switch (action.type) {
    case 'SELECT_CATEGORY':
      return action.selectedCategory;
    default:
      return state
  }
};