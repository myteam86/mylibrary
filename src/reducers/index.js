import {combineReducers} from 'redux';
import { routerReducer } from 'react-router-redux'
import booksById from './booksById';
import listByCategory from './listByCategory';
import categories from './categories';
import {activePage} from './activePage';
import {selectedCategory} from './selectedCategory';
import book from './book';
import profile from './profile';


const rootReducer = combineReducers({
  routing: routerReducer,
  activePage,
  selectedCategory,
  booksById,
  listByCategory,
  categories,
  book,
  profile
});


export default rootReducer