import {normalize} from 'normalizr';
import * as schemas from './schemas';
import * as constants from '../utils/constants';
import _ from 'lodash';
import browserStore from 'store2';

const BASE_URL = '/api/v1';

export function selectPage(selectedPage) {
  return {
    type: 'SELECT_PAGE',
    selectedPage
  }
}

export function selectCategory(selectedCategory) {
  return {
    type: 'SELECT_CATEGORY',
    selectedCategory
  }
}

export const fetchCategories = ()=> (dispatch, getState)=> {
  let categories = getState().categories;
  if (categories.length) return false;
  dispatch({
    type: 'FETCH_REQUEST',
  });
  let url = `${BASE_URL}/categories`;
  return fetch(url)
    .then(response => response.json())
    .then(
      response => dispatch({
        type: 'FETCH_CATEGORIES_SUCCESS',
        response
      })
      , error => dispatch(
        {type: 'FETCH_ERROR', error: error}
      )
    )
};

export const fetchBookDetails = (id)=> (dispatch, getState)=> {
  dispatch({
    type: 'FETCH_REQUEST',
  });
  let url = `${BASE_URL}/books/${id}`;
  return fetch(url)
    .then(response => response.json())
    .then(
      response => dispatch({
        type: 'FETCH_BOOK_SUCCESS',
        response
      })
      , error => dispatch(
        {type: 'FETCH_ERROR', error: error}
      )
    )
};

export const fetchBooks = (activePage, selectedCategory) => (dispatch, getState) => {
  let propPath = `${selectedCategory}.${activePage}`;
  let listByCategory = getState().listByCategory;
  if (_.has(listByCategory, propPath)) {
    return false;
  }
  dispatch({
    type: 'FETCH_REQUEST',
  });

  let limit = constants.BOOKS_PER_PAGE;
  let url = `${BASE_URL}/books?limit=${limit}&page=${activePage}`;
  if (selectedCategory && selectedCategory !== constants.ALL_CATEGORIES) {
    url += `&category_id=${selectedCategory}`
  }
  return fetch(url)
    .then(response => response.json())
    .then(
      response => dispatch({
        type: 'FETCH_BOOKS_SUCCESS',
        activePage,
        selectedCategory,
        response: normalize(response.data, schemas.arrayOfBooks),
        totalItems: response.total
      })
      , error => dispatch(
        {type: 'FETCH_ERROR', error: error}
      )
    )
};


export const login = (email, password) => (dispatch, getState)=> {
  let body = {email: email, password: password};
  dispatch({
    type: 'LOGIN_REQUEST'
  });
  fetch(`${BASE_URL}/authenticate`,
    {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {"Content-Type": 'application/json'}
    }
  )
    .then((response) => {
      if (response.ok) return response.json()
    })
    .then((response)=> {
        if (response) {
          browserStore.set(constants.JWT_TOKEN_STORAGE_KEY, response.token);
          dispatch({
            type: 'LOGIN_SUCCESS'
          });
          fetchUserDetails(dispatch)
        }
      }
      , error => dispatch(
        {type: 'FETCH_ERROR', error: error}
      )
    )
};

export const logout = ()=> {
  browserStore.remove(constants.JWT_TOKEN_STORAGE_KEY);
  return {type: 'LOG_OUT'}
};

export const getAuthenticatedUser = () =>(dispatch, getState) => {
  fetchUserDetails(dispatch)
};


const fetchUserDetails = (dispatch) => {
  dispatch({
    type: 'FETCH_REQUEST'
  });
  let token = browserStore.get(constants.JWT_TOKEN_STORAGE_KEY);
  if (!token) return false;

  let options = {
    headers: {
      Authorization: `Bearer ${token}`
    }
  };
  fetch(`${BASE_URL}/getAuthenticatedUser`, options)
    .then((response) => {
      if (!response.ok) {
        browserStore.remove(constants.JWT_TOKEN_STORAGE_KEY);
      } else {
        return response.json()
      }
    })
    .then((response) => {
      dispatch({
        type: 'FETCH_AUTHENTICATION_SUCCESS',
        response: response
      });
    })
  ;
};