import _ from 'lodash';

const listByCategory = (state = {}, action) => {
  if (!_.has(action, 'response.result')) {
    return state;
  }
  let {activePage, selectedCategory, totalItems, response} = action;
  let categoryData = Object.assign({}, state[selectedCategory]);
  categoryData = Object.assign(categoryData, {
    [activePage]: response.result,
    totalItems
  });

  return {
    ...state,
    [selectedCategory]: categoryData
  };
};

export default listByCategory;

export const getData = (state, category, page) => {
  let propPath = `${category}.${page}`;
  if (!_.has(state, propPath)) {
    return {
      items: [],
      totalItems: 0
    };
  }
  return {
    items: state[category][page],
    totalItems: state[category].totalItems
  }
};