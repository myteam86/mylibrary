<?php
namespace App\Repositories;

abstract class BaseRepository
{

	/**
	 * The Model instance.
	 *
	 * @var \Illuminate\Database\Eloquent\Model
	 */
	protected $model;

	/**
	 * Get collection of all resource.
	 *
	 * @return \Illuminate\Support\Collection
	 */
	public function getAll()
	{
		return $this->model->all();

	}

	/**
	 * Get number of records.
	 *
	 * @return array
	 */
	public function getNumber()
	{
		$total = $this->model->count();

		$new = $this->model->whereSeen(0)->count();

		return compact('total', 'new');
	}

	/*
	* Get specific resource details
	*
	* @param int $id
	* @param array $populates
	* @return array $book
	*/
	public function getById($id, $populates)
	{
		$builder = $this->model->where('id', $id);
		foreach ($populates as $item) {
			$builder = $builder->with($item);
		}
		return $builder->firstOrFail();
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 * @param  array $inputs
	 * @return array $book
	 */
	public function insertOrUpdate($id, $inputs)
	{
		if ($id) {
			$_model = $this->model->where('id', $id)->firstOrFail();
		} else {
			$_model = new $this->model;
		}
		foreach ($inputs as $key => $value) {
			$_model->{$key} = $inputs[$key];
		}
		$_model->save();
		return $_model;
	}

	/*
	 *Delete the specific resource in storage
	 *
	 * @param int $in
	 * @return bool result
	 */
	public function destroy($id)
	{
		$_model = $this->model->where('id', $id)->firstOrFail();
		return $_model->delete();
	}

}