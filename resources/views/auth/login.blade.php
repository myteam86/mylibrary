@extends('front.template')

@section('head')
  {!! HTML::style('css/main.css') !!}
@stop

@section('main')
  <div class="middle-wrapper">
    <div class="middle-container">
      <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
          <div class="panel panel-default col-lg-12">
            <h2 class="intro-text text-center">{{ trans('front/login.connection') }}</h2>
            <p class="text-center">{{ trans('front/login.text') }}</p>
            <hr>
            <form action="/auth/login" method="post">
              <div class="form-group">
                <input type="text" class="form-control" id="log" name="log"
                       placeholder="{{trans('front/login.usernameOrEmail')}}"/>
              </div>
              <div class="form-group">
                <input type="password" class="form-control" id="password" name="password"
                       placeholder="{{trans('front/login.password')}}"/>
              </div>
              <div class="form-group">
                {{ csrf_field() }}
                <button type="submit"
                        class="btn btn-primary btn-block">{{trans('front/login.submit')}}</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop

