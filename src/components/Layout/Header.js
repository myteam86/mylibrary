import React from 'react';
import Navigation from './Navigation';
import {Navbar} from 'react-bootstrap'
import s from './Header.scss';

class Header extends React.Component {

  render() {
    return (
      <header className={`${s.header}`} ref={node => (this.root = node)}>
        <Navbar inverse>
          <Navbar.Header>
            <Navbar.Brand>
              <a href="/#"><h2 className={`${s.title}`}>Center Library</h2></a>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse>
            <Navigation />
          </Navbar.Collapse>
        </Navbar>
      </header>
    );
  }

}

export default Header;
