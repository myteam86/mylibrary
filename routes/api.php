<?php


$api = app('Dingo\Api\Routing\Router');

$api->version(['v1'], function ($api) {

  /*Book Resource*/
  $api->get('v1/books/{id}', 'App\Http\Controllers\api\v1\BookController@show');
  $api->get('v1/books', 'App\Http\Controllers\api\v1\BookController@index');
  $api->put('v1/books/{id}', 'App\Http\Controllers\api\v1\BookController@update');
  $api->post('v1/books', 'App\Http\Controllers\api\v1\BookController@store');
  $api->delete('v1/books/{id}', 'App\Http\Controllers\api\v1\BookController@destroy');

  /*User Resource*/
  $api->get('v1/users', 'App\Http\Controllers\api\v1\UserController@index');
	$api->get('v1/users/{id}', 'App\Http\Controllers\api\v1\UserController@show');
//	$api->put('v1/books/{id}', 'App\Http\Controllers\api\v1\BookController@update');
//	$api->post('v1/books', 'App\Http\Controllers\api\v1\BookController@store');
//	$api->delete('v1/books/{id}', 'App\Http\Controllers\api\v1\BookController@destroy');

  /*Category Resource*/
  $api->get('v1/categories', 'App\Http\Controllers\api\v1\CategoryController@getAll');

  /*Authentication*/
  $api->post('v1/authenticate', 'App\Http\Controllers\api\v1\AuthController@authenticate');
});

$api->version(['v1'],['middleware' => 'verifyJWT'], function ($api) {

  /*Authentication*/
  $api->get('v1/getAuthenticatedUser', 'App\Http\Controllers\api\v1\AuthController@getAuthenticatedUser');
});