export const activePage = (state = 1, action) => {
  switch (action.type) {
    case 'SELECT_PAGE':
      return action.selectedPage;
    default:
      return state
  }
};
