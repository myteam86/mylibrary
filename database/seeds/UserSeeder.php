<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Role,
  App\Models\User;
use Illuminate\Support\Facades\DB;

//use App\Services\LoremIpsumGenerator;

class UserSeeder extends Seeder
{

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Model::unguard();
    $user_id_1 = 1;
    $user_id_2 = 2;
    $user_id_3 = 3;

    $role_id_1 = 1;
    $role_id_2 = 2;
    $role_id_3 = 3;

    Role::create([
      'id' => $role_id_1,
      'display_name' => 'Administrator',
      'name' => 'admin'
    ]);

    Role::create([
      'id' => $role_id_2,
      'display_name' => 'Librarian',
      'name' => 'libr'
    ]);

    Role::create([
      'id' => $role_id_3,
      'display_name' => 'User',
      'name' => 'user'
    ]);

    User::create([
      'id' => $role_id_1,
      'username' => 'GreatAdmin',
      'email' => 'admin@abc.com',
      'password' => bcrypt('admin'),
      'valid' => true
    ]);

    User::create([
      'id' => $role_id_2,
      'username' => 'GreatLibrarian',
      'email' => 'libr@abc.com',
      'password' => bcrypt('redac'),
      'valid' => true
    ]);

    User::create([
      'id' => $role_id_3,
      'username' => 'Walker',
      'email' => 'walker@gmail.com',
      'password' => bcrypt('walker'),
      'valid' => true
    ]);

    DB::insert('insert into role_user (user_id, role_id) values (?, ?)', [$user_id_1, $role_id_1]);
    DB::insert('insert into role_user (user_id, role_id) values (?, ?)', [$user_id_2, $role_id_2]);
    DB::insert('insert into role_user (user_id, role_id) values (?, ?)', [$user_id_3, $role_id_3]);
  }

}