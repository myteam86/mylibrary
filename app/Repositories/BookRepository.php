<?php

namespace App\Repositories;

use App\Models\Book;

class BookRepository extends BaseRepository
{

    /**
     * Create a new BookRepository instance.
     *
     * @param  Book $model
     */
    public function __construct(Book $model)
    {
        $this->model = $model;
    }

    /**
     * Get resource collection paginate.
     *
     * @param  int $category_id
     * @param  int $limit
     * @param  int $page
     * @return Illuminate\Support\Collection
     */
    public function index($limit, $page, $category_id)
    {
        $offset = ($page - 1) * $limit;
        $builder = $this->model
            ->with('location')
            ->with('category');
        if ($category_id) {
            $builder->when('category', function ($query) use ($category_id) {
                $query->where('category_id', $category_id);
            });
        }
        return $builder->skip($offset)
            ->take($limit)
            ->paginate($limit);

    }
}