import _ from 'lodash';
import * as Constants from '../utils/constants';

const book = (state = {}, action) => {
  switch (action.type) {
    case 'FETCH_AUTHENTICATION_SUCCESS':
      return action.response ? action.response.user : {};
    case 'LOG_OUT':
      return {};
    default:
      return state
  }
};

export default book;

export const hasLibrarianRole = (state)=> {
  let roles = state.roles;
  if (!roles) {
    return false
  }
  if (!roles.length) {
    return false
  }
  let index = _.findIndex(roles, (item)=> {
    return item.name === Constants.LIBRARIAN_ROLE || item.name === Constants.ADMIN_ROLE
  });

  return index !== -1;
};