module.exports = {
  ALL_CATEGORIES: 'all',
  BOOKS_PER_PAGE: 10,
  JWT_TOKEN_STORAGE_KEY: 'jwtToken',
  LIBRARIAN_ROLE: 'libr',
  ADMIN_ROLE: 'admin'

};