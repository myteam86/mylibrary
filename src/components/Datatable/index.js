import React, {PropTypes} from 'react';
import {Col, Table, Row, Pagination} from 'react-bootstrap'
import moment from 'moment'
import s from './styles.scss'

const formatDate = (date, $format) => {
  if (!date) {
    return ''
  }
  let _format = $format || 'YYYY-MM-DD';
  if (this.props.value) {
    date = moment(this.props.value, _format).format('DD/MM/YYYY');
  }
  return (<span>{date}</span>)
}


const createLink = (id, text, makeLink) => {
  let href = makeLink(id);
  return (
      <a href={href}>{text}</a>
  )
};

class Datatable extends React.Component {

  constructor(props) {
    super(props);
  }

  static propTypes = {
    handleOnPageSelect: PropTypes.func,
  };

  render() {
    var thead = this.props.thead;
    var tbody = this.props.tbody;
    var metadata = this.props.metadata;
    var name = this.props.name;

    return (
        <Row>
          <Col md={6} xs={12}><h4>{name}</h4></Col>
          <Col xs={12}>
            <Table striped condensed hover bordered>
              <thead>
              <tr>
                <th>#</th>
                {
                  thead.map((item, i)=><th key={item.key + i}>{item.title}</th>)
                }
              </tr>
              </thead>
              <tbody>
              {
                tbody.length == 0
                    ?
                    <tr>
                      <td colSpan={thead.length + 1} className="text-center"> Không có dữ liệu!</td>
                    </tr>
                    :
                    tbody.map(
                        (itemBody, iBody)=>
                            <tr key={(itemBody.id) + iBody}>
                              <td>{iBody + (parseInt(metadata.page) - 1) * metadata.limit + 1}</td>
                              {
                                thead.map(
                                    (itemThead, iThead) =>
                                        itemThead.type == 'datetime' ?
                                            <td key={itemThead.key + iThead}>
                                              {formatDate(itemBody[itemThead.key])}
                                            </td>
                                            : itemThead.type == 'link' ?
                                            <td key={itemThead.key + iThead}>
                                              {createLink(itemBody.id, itemBody[itemThead.key], itemThead.makeLink)}
                                            </td>
                                            : <td key={itemThead.key + iThead}>{itemBody[itemThead.key]}</td>
                                )
                              }
                            </tr>
                    )
              }
              </tbody>
            </Table>
          </Col>
          <Col md={6} xs={12} mdOffset={6} xsOffset={0}>
            <div className={s.pagination}>
              <Pagination
                  className='pull-right'
                  prev
                  next
                  first
                  last
                  ellipsis
                  boundaryLinks
                  items={metadata.totalPages}
                  maxButtons={metadata.maxButtons}
                  activePage={metadata.page}
                  onSelect={this.props.onPageSelect}/>
            </div>
          </Col>
        </Row>
    )
  }
}
;

export default Datatable;
