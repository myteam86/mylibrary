import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {Row, Col, Grid} from 'react-bootstrap';
import classNames from 'classnames'
import Layout from '../../components/Layout';
import Datatable from '../../components/Datatable';
import s from './styles.scss';
import * as actions from '../../actions';
import * as constants from '../../utils/constants';
import * as fromListByCategory from '../../reducers/listByCategory';

class HomePage extends React.Component {
  static propTypes = {
    books: PropTypes.array,
    categories: PropTypes.array,
    totalItems: PropTypes.number,
    activePage: PropTypes.number,
    selectedCategory: PropTypes.any,
  };

  constructor(props) {
    super(props);
    this.handleOnPageSelect = this.handleOnPageSelect.bind(this);
    this.handleOnCategoryClick = this.handleOnCategoryClick.bind(this)
  }
  
  componentDidMount(){
    const {activePage, selectedCategory, fetchBooks, fetchCategories} = this.props;
    fetchBooks(activePage, selectedCategory);
    fetchCategories();
  }

  componentWillReceiveProps(nextProps) {
    const {activePage, selectedCategory, fetchBooks, selectPage} = nextProps;
    if (
        (nextProps.activePage !== this.props.activePage)
        || (nextProps.selectedCategory !== this.props.selectedCategory && nextProps.activePage === 1)
    ) {
      fetchBooks(activePage, selectedCategory)
    }
    if (nextProps.selectedCategory !== this.props.selectedCategory) {
      selectPage(1)
    }
  }

  handleOnPageSelect(activePage) {
    const {selectPage} = this.props;
    selectPage(activePage)
  }

  handleOnCategoryClick(selectedCategory) {
    const {selectCategory} = this.props;
    selectCategory(selectedCategory)
  }

  render() {
    const makeLink = (id) => (`#/book/details/${id}`);
    //TODO: check is fetching data to not render list
    let {categories, books, totalItems, activePage, selectedCategory} = this.props;
    let thead = [
      {title: 'Title', key: 'title', type: 'link', makeLink: makeLink},
      {title: 'Author', key: 'author'},
      {title: 'Publisher', key: 'publisher'},
      {title: 'Year', key: 'year'}
    ];
    let tableName = 'Books List';
    let limit = constants.BOOKS_PER_PAGE;
    let tableMetadata = {
      limit: limit,
      totalPages: Math.ceil(totalItems / limit),
      page: activePage,
      maxButtons: 3
    };
    let all = constants.ALL_CATEGORIES;
    let activeClass = s.active;

    const handleOnCategoryClick = this.handleOnCategoryClick;

    return (
        <Layout className={s.content}>
          <Grid>
            <Row>
              <Col md={2} xs={12}>
                <h4>Category</h4>
                <ul>
                  <li >
                    <p className={`${s.pointer} ${classNames({[activeClass]: selectedCategory === 'all' })}`} onClick={() => handleOnCategoryClick(all)}>All Categories</p>
                  </li>
                  {categories.map((cat) =>
                      <li key={cat.id + cat.name}>
                        <p className={`${s.pointer} ${classNames({[activeClass]: selectedCategory === cat.id })}`} onClick={() => handleOnCategoryClick(cat.id)}>{cat.name}</p>
                      </li>
                  )}
                </ul>
              </Col>
              <Col className={s.list} md={10} xs={12}>
                <Datatable
                    name={tableName}
                    thead={thead}
                    metadata={tableMetadata}
                    tbody={books}
                    onPageSelect={this.handleOnPageSelect}
                />
              </Col>
            </Row>
          </Grid>
        </Layout>
    );
  }
}

function mapStateToProps(state) {
  const {selectedCategory, activePage, booksById, listByCategory, categories} = state;
  let {items, totalItems} = fromListByCategory.getData(listByCategory, selectedCategory, activePage)
  let books = items.map(id => booksById[id]);
  return {
    selectedCategory,
    activePage,
    books,
    categories,
    totalItems
  }
}

export default connect(mapStateToProps, actions)(HomePage);
