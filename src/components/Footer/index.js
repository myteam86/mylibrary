import React from 'react';
const s = require('./Footer.scss');

function Footer() {
  return (
    <footer >
      <div className={`${s.cont}`}>
        <div className="container">
          <p>Designed and built with all the love in the world by ABC</p>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
