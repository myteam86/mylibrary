export BookDetails from './book-details';
export Home from './home';
export Login from './login';
export BookManagement from './book-management/list';
export EditBook from './book-management/edit';