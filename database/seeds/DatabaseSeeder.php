<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use
  App\Models\Book,
  App\Models\Location,
  App\Models\Category;
use App\Models\Role,
  App\Models\User;
use Illuminate\Support\Facades\DB;

//use App\Services\LoremIpsumGenerator;

class DatabaseSeeder extends Seeder
{

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Model::unguard();

    Book::create([
      'title' => 'Laravel 5 Essentials',
      'summary' => 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus',
      'author' => 'Martin Bean',
      'publisher' => 'Packt Publishing',
      'year' => 2015,
      'quantity' => 3,
      'location_id' => 1,
      'category_id' => 1
    ]);

    Book::create([
      'title' => 'React.js Essentials',
      'summary' => 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus',
      'author' => 'Artemij Fedosejev',
      'publisher' => 'Packt Publishing',
      'year' => 2015,
      'quantity' => 3,
      'location_id' => 1,
      'category_id' => 1
    ]);

    Book::create([
      'title' => 'Twilight',
      'summary' => 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus',
      'author' => 'Stephenie Meyer',
      'publisher' => 'Little, Brown and Company',
      'year' => 2005,
      'quantity' => 5,
      'location_id' => 2,
      'category_id' => 2
    ]);

    Book::create([
      'title' => 'Twilight',
      'summary' => 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus',
      'author' => 'Stephenie Meyer',
      'publisher' => 'Little, Brown and Company',
      'year' => 2005,
      'quantity' => 5,
      'location_id' => 2,
      'category_id' => 2
    ]);

    Book::create([
      'title' => 'Breaking Dawn',
      'summary' => 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus',
      'author' => 'Stephenie Meyer',
      'publisher' => 'Little, Brown and Company',
      'year' => 2005,
      'quantity' => 5,
      'location_id' => 2,
      'category_id' => 2
    ]);

    Book::create([
      'title' => 'New Moon',
      'summary' => 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus',
      'author' => 'Stephenie Meyer',
      'publisher' => 'Little, Brown and Company',
      'year' => 2005,
      'quantity' => 5,
      'location_id' => 2,
      'category_id' => 2
    ]);

    Book::create([
      'title' => 'Eclipse',
      'summary' => 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus',
      'author' => 'Stephenie Meyer',
      'publisher' => 'Little, Brown and Company',
      'year' => 2005,
      'quantity' => 5,
      'location_id' => 2,
      'category_id' => 2
    ]);

    Book::create([
      'title' => 'Laravel 5 Essentials (clone 1)',
      'summary' => 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus',
      'author' => 'Martin Bean',
      'publisher' => 'Packt Publishing',
      'year' => 2015,
      'quantity' => 3,
      'location_id' => 1,
      'category_id' => 1
    ]);

    Book::create([
      'title' => 'React.js Essentials (clone 1)',
      'summary' => 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus',
      'author' => 'Artemij Fedosejev',
      'publisher' => 'Packt Publishing',
      'year' => 2015,
      'quantity' => 3,
      'location_id' => 1,
      'category_id' => 1
    ]);

    Book::create([
      'title' => 'Twilight (clone 1)',
      'summary' => 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus',
      'author' => 'Stephenie Meyer',
      'publisher' => 'Little, Brown and Company',
      'year' => 2005,
      'quantity' => 5,
      'location_id' => 2,
      'category_id' => 2
    ]);

    Book::create([
      'title' => 'Twilight (clone 2)',
      'summary' => 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus',
      'author' => 'Stephenie Meyer',
      'publisher' => 'Little, Brown and Company',
      'year' => 2005,
      'quantity' => 5,
      'location_id' => 2,
      'category_id' => 2
    ]);

    Book::create([
      'title' => 'Breaking Dawn (clone 1)',
      'summary' => 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus',
      'author' => 'Stephenie Meyer',
      'publisher' => 'Little, Brown and Company',
      'year' => 2005,
      'quantity' => 5,
      'location_id' => 2,
      'category_id' => 2
    ]);

    Book::create([
      'title' => 'New Moon (clone 1)',
      'summary' => 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus',
      'author' => 'Stephenie Meyer',
      'publisher' => 'Little, Brown and Company',
      'year' => 2005,
      'quantity' => 5,
      'location_id' => 2,
      'category_id' => 2
    ]);

    Book::create([
      'title' => 'Eclipse (clone 1)',
      'summary' => 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus',
      'author' => 'Stephenie Meyer',
      'publisher' => 'Little, Brown and Company',
      'year' => 2005,
      'quantity' => 5,
      'location_id' => 2,
      'category_id' => 2
    ]);

    Location::create([
      'name' => '1.0.0',
      'note' => ''
    ]);
    Location::create([
      'name' => '2.0.0',
      'note' => ''
    ]);
    Category::create([
      'name' => 'Programming',
      'description' => 'Cras sit amet nibh libero'
    ]);
    Category::create([
      'name' => 'Novel',
      'description' => 'Cras sit amet nibh libero'
    ]);

    $user_id_1 = 1;
    $user_id_2 = 2;
    $user_id_3 = 3;

    $role_id_1 = 1;
    $role_id_2 = 2;
    $role_id_3 = 3;

    Role::create([
      'id' => $role_id_1,
      'display_name' => 'Administrator',
      'name' => 'admin'
    ]);

    Role::create([
      'id' => $role_id_2,
      'display_name' => 'Librarian',
      'name' => 'libr'
    ]);

    Role::create([
      'id' => $role_id_3,
      'display_name' => 'User',
      'name' => 'user'
    ]);

    User::create([
      'id' => $role_id_1,
      'username' => 'GreatAdmin',
      'email' => 'admin@abc.com',
      'password' => bcrypt('admin'),
      'valid' => true
    ]);

    User::create([
      'id' => $role_id_2,
      'username' => 'GreatLibrarian',
      'email' => 'libr@abc.com',
      'password' => bcrypt('redac'),
      'valid' => true
    ]);

    User::create([
      'id' => $role_id_3,
      'username' => 'Walker',
      'email' => 'walker@gmail.com',
      'password' => bcrypt('walker'),
      'valid' => true
    ]);

    DB::insert('insert into role_user (user_id, role_id) values (?, ?)', [$user_id_1, $role_id_1]);
    DB::insert('insert into role_user (user_id, role_id) values (?, ?)', [$user_id_2, $role_id_2]);
    DB::insert('insert into role_user (user_id, role_id) values (?, ?)', [$user_id_3, $role_id_3]);
  }

}