import { Schema, arrayOf } from 'normalizr';

export const book = new Schema('books');
export const arrayOfBooks = arrayOf(book);
