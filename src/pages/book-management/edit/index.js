import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {Row, Col, Grid, Media, Label} from 'react-bootstrap'
import Layout from '../../../components/Layout';
import s from './styles.scss';


class HomePage extends React.Component {
  static propTypes = {};

  constructor(props) {
    super(props);
  }

  render() {
    let {book} = this.props;
    return (
        <Layout className={s.content}>
          <Grid>
            <Row>
              <Col md={8}  mdOffset={2} xs={12}  xsOffset={0}>
                <h3>{book.title}</h3>
                <Media>
                  <Media.Left align="top">
                    <img className={s.cover}
                        src="https://cdn.pastemagazine.com/www/system/images/photo_albums/hobbit-book-covers/large/photo_5653_0.jpg?1384968217"
                        alt="..."/>
                  </Media.Left>
                  <Media.Body>
                    <Media.Heading>Book Description</Media.Heading>
                    <p>{book.summary}</p>
                    <h4>Book Details</h4>
                    <p>Publisher: {book.publisher}</p>
                    <p>By: {book.author}</p>
                    <p>Year: {book.year}</p>
                    <p>Bookshelf: {book.location.name}</p>
                    <p>
                      {
                        book.quantity > 0
                            ? <Label bsStyle="success">Available</Label>
                            : <Label bsStyle="danger">Not Available</Label>
                      }
                    </p>
                  </Media.Body>
                </Media>
              </Col>
            </Row>
          </Grid>
        </Layout>
    );
  }
}

function mapStateToProps(state) {
  let {book} = state;
  return {book}
}

const initStateWithResponseData = (responseData) => {
  const state = {
    book: responseData.book.book
  };
  return state;
};

export default connect(mapStateToProps)(HomePage);