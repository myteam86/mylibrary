import React from 'react';
import {connect} from 'react-redux';
import * as actions  from './actions';

class App extends React.Component {

  componentDidMount() {
    let {profile, getAuthenticatedUser} = this.props;
    if (!profile.id) {
      getAuthenticatedUser();
    }
  }

  render() {
    let {children} = this.props;
    return (
      <div>{children}</div>
    );
  }
}

const mapStateToProps = (state, ownProps)=> {
  let {profile} = state;
  let {children} = ownProps;
  return {profile, children};
};

export default connect(mapStateToProps, actions)(App);
