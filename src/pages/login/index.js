import React, {PropTypes} from 'react';
import ReactDOM from 'react-dom';
import {connect} from 'react-redux';
import Layout from '../../components/Layout';
import s from './styles.scss';
import {Row, Col, Grid, Panel, FormGroup, FormControl, ButtonGroup, Button} from 'react-bootstrap';
import * as actions from '../../actions';


class LoginPage extends React.Component {
  static propTypes = {};

  constructor(props) {
    super(props);
    this.submit = this.submit.bind(this);
  }

  submit() {
    let email = ReactDOM.findDOMNode(this.emailInput).value;
    let password = ReactDOM.findDOMNode(this.passwordInput).value;
    if (email && password) {
      this.props.login(email, password);
    }
  }

  render() {
    return (
      <Layout>
        <Grid>
          <Row>
            <Col md={6} mdOffset={3} xs={12} xsOffset={0}>
              <Panel bsStyle="default">
                <h2 className="text-center">Connection</h2>
                <p className="text-center"> To log on this site you must fill this form</p>
                <hr/>
                <form>
                  <FormGroup controlId="emailInput">
                    <FormControl type="text" placeholder="Your email" ref={(ref) => this.emailInput = ref}/>
                  </FormGroup>
                  <FormGroup>
                    <FormControl type="password" placeholder="Your password" ref={(ref) => this.passwordInput = ref}/>
                  </FormGroup>
                  <ButtonGroup vertical block>
                    <Button onClick={this.submit} bsStyle="primary">Submit</Button>
                  </ButtonGroup>
                </form>

              </Panel>

            </Col>
          </Row>
        </Grid>
      </Layout>
    );
  }
}

function mapStateToProps(state) {
  return {isLogged: state.isLogged}
}

export default connect(mapStateToProps, actions)(LoginPage);