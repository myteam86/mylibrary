import 'babel-polyfill';
import 'whatwg-fetch';
import FastClick from 'fastclick';
import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux'
import {syncHistoryWithStore} from 'react-router-redux'
import {hashHistory} from 'react-router'
import configureStore from './src/store'
import Routes from './src/Routes'

const store = configureStore();
const history = syncHistoryWithStore(hashHistory, store);

ReactDOM.render(
  <Provider store={store}>
      <Routes history={history}/>
  </Provider>
  , document.getElementById('container')
);

// Eliminates the 300ms delay between a physical tap
// and the firing of a click event on mobile browsers
// https://github.com/ftlabs/fastclick
FastClick.attach(document.body);

