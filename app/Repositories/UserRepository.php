<?php

namespace App\Repositories;

use App\Models\User, App\Models\Role;

class UserRepository extends BaseRepository
{
  /**
   * The User instance.
   *
   * @var \App\Models\User
   */
  protected $user;

  /**
   * The Role instance.
   *
   * @var \App\Models\Role
   */
  protected $role;

  /**
   * Create a new UserRepository instance.
   *
   * @param \App\Models\User $user
   * @param \App\Models\Role $role
   */
  public function __construct(
    User $user,
    Role $role)
  {
    $this->model = $user;
    $this->role = $role;
  }

  /**
   * Get users collection paginate.
   *
   * @param  int $limit
   * @param  int $page
   * @return \Illuminate\Support\Collection
   */
  public function index($limit, $page)
  {
    $offset = ($page - 1) * $limit;
    return $this->model
      ->oldest('seen')
      ->latest()
      ->skip($offset)
      ->take($limit)
      ->paginate($limit);
  }

  /**
   * Create a user.
   *
   * @param  array $inputs
   * @return \App\Models\User
   * @internal param int $confirmation_code
   */
  public function store($inputs)
  {
    $user = new $this->model;

    $user->password = bcrypt($inputs['password']);

    $this->save($user, $inputs);

    return $user;
  }

}