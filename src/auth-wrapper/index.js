import {UserAuthWrapper} from 'redux-auth-wrapper';
import {routerActions} from 'react-router-redux'
import _ from 'lodash';
import {hasLibrarianRole} from '../reducers/profile'

export const VisibleOnlyLogged = UserAuthWrapper({
  wrapperDisplayName: 'VisibleOnlyLogged',
  authSelector: state => state.profile,
  predicate: profile => !_.isEmpty(profile),
  FailureComponent: null
});

export const VisibleOnlyLoggedOut = UserAuthWrapper({
  wrapperDisplayName: 'VisibleOnlyLoggedOut',
  authSelector: state => state.profile,
  predicate: profile => _.isEmpty(profile),
  FailureComponent: null
});

export const visibleOnlyLibrarian = UserAuthWrapper({
  wrapperDisplayName: 'visibleOnlyLibrarian',
  authSelector: state => state.profile,
  predicate: profile => hasLibrarianRole(profile),
  FailureComponent: null
});

export const onlyLibrarian = UserAuthWrapper({
  wrapperDisplayName: 'onlyLibrarian',
  authSelector: state => state.profile,
  predicate: profile => hasLibrarianRole(profile),
  failureRedirectPath: '#/login'
});

export const UserIsAuthenticated = UserAuthWrapper({
  wrapperDisplayName: 'UserIsAuthenticated', // a nice name for this auth check
  authSelector: state => state.profile, // how to get the user state
  redirectAction: routerActions.replace, // the redux action to dispatch for redirect
});

export const UserNotAuthenticated = UserAuthWrapper({
  wrapperDisplayName: 'UserNotAuthenticated', // a nice name for this auth check
  authSelector: state => state.profile, // how to get the user state
  predicate: profile => !profile.id,
  failureRedirectPath: "#/"
});