<?php

return [
    'connection' => 'Connection',
    'text' => 'To log on this site you must fill this form',
    'usernameOrEmail' => 'Your username or email',
    'password' => 'Your password',
    'submit' => 'submit',
];