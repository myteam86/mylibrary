<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ trans('front/site.title') }}</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  @yield('head')

  {!! HTML::style('css/bootstrap.min.css') !!}
  {!! HTML::style('css/bootstrap-theme.min.css') !!}
  {!! HTML::style('http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800') !!}
  {!! HTML::style('http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic') !!}
</head>
<body>
<header role="contentinfo">
  @yield('header')
</header>

<main role="main" class="container">
  @yield('main')
</main>

<footer role="contentinfo">
  @yield('footer')
</footer>

@yield('scripts')
</body>
</html>