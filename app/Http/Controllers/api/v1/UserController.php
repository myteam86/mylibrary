<?php

namespace App\Http\Controllers\api\v1;

use Illuminate\Http\Request,
  App\Http\Requests,
  App\Repositories\UserRepository,
  App\Http\Controllers\Controller;

class UserController extends Controller
{
  protected $userRepository;

  /**
   * Create a new BookController instance.
   *
   * @param  UserRepository $userRepository
   */
  public function __construct(UserRepository $userRepository)
  {
    $this->userRepository = $userRepository;
  }

  /**
   * Display a listing of the resource.
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $input = $request->all();
    if (isset($input['limit'])) {
      $limit = $input['limit'];
    } else {
      $limit = 30;
    }
    if (isset($input['page'])) {
      $page = $input['page'];
    } else {
      $page = 1;
    }
    $user = $this->userRepository->index($limit, $page);
    return $user;
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $inputs = $request->all();
    if (isset($inputs["id"])) {
      $inputs["id"] = null;
    }
    $book = $this->userRepository->insertOrUpdate(null, $inputs);
    return $book;
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $user = $this->userRepository->getById($id, array('roles'));
    return $user;
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $inputs = $request->all();
    if (isset($inputs["id"])) {
      $inputs["id"] = null;
    }
    $booK = $this->bookRepository->insertOrUpdate($id, $inputs);
    return $booK;
  }

}
