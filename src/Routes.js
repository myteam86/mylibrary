import React from 'react';
import {Router, Route, IndexRoute} from 'react-router';
import {BookDetails, BookManagement, EditBook, Home, Login} from './pages';
import App from './App';
import {UserNotAuthenticated, onlyLibrarian} from './auth-wrapper';

class Routes extends React.Component {

  render() {
    let {history} = this.props;
    return (
      <Router history={history}>
        <Route path="/" component={App}>
          <IndexRoute component={Home}/>
          <Route path="#/" component={Home}/>
          <Route path="login" component={UserNotAuthenticated(Login)}/>
          <Route path="book/details/:id" component={BookDetails}/>
          <Route path="management/books" component={onlyLibrarian(BookManagement)}/>
          <Route path="management/book/edit/:id" component={onlyLibrarian(EditBook)}/>
        </Route>
      </Router>
    );
  }

}

export default Routes;
