<?php

namespace App\Repositories;

use App\Models\Category;

class CategoryRepository extends BaseRepository
{

    /**
     * Create a new BookRepository instance.
     *
     * @param  Category $model
     */
    public function __construct(Category $model)
    {
        $this->model = $model;
    }
}