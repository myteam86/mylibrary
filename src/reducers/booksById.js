import _ from 'lodash';

const booksById = (state = {}, action) => {
  if (!_.has(action, 'response.entities')) {
    return state;
  }
  //TODO: improve check whether books prop exists and handle case of not exist
  return {
    ...state,
    ...action.response.entities.books,
  };
};

export default booksById;

export const getBook = (state, id) => state[id];