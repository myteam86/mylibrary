<?php

namespace App\Http\Controllers\api\v1;

use Illuminate\Http\Request,
  App\Http\Requests,
  App\Repositories\BookRepository,
  App\Http\Controllers\Controller;

class BookController extends Controller
{
  protected $bookRepository;

  /**
   * Create a new BookController instance.
   *
   * @param  BookRepository $bookRepository
   */
  public function __construct(BookRepository $bookRepository)
  {
    $this->bookRepository = $bookRepository;
  }

  /**
   * Display a listing of the resource.
   * @param Request $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $input = $request->all();
    if (isset($input['limit'])) {
      $limit = $input['limit'];
    } else {
      $limit = 30;
    }
    if (isset($input['page'])) {
      $page = $input['page'];
    } else {
      $page = 1;
    }
    if (isset($input['category_id'])) {
      $category_id = $input['category_id'];
    } else {
      $category_id = null;
    }
    $books = $this->bookRepository->index($limit, $page, $category_id);
    return $books;
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $inputs = $request->all();
    if (isset($inputs["id"])) {
      $inputs["id"] = null;
    }
    $book = $this->bookRepository->insertOrUpdate(null, $inputs);
    return $book;
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $book = $this->bookRepository->getById($id, array('location', 'category'));
    return $book;
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $inputs = $request->all();
    if (isset($inputs["id"])) {
      $inputs["id"] = null;
    }
    $booK = $this->bookRepository->insertOrUpdate($id, $inputs);
    return $booK;
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $result = $this->bookRepository->destroy($id);
    if ($result) {
      return 'Deleted';
    } else {
      $this->throwHttpException();
    }

  }
}
