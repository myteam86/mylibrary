<?php

namespace App\Http\Middleware;

use Tymon\JWTAuth\Middleware\BaseMiddleware;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class VerifyJWTToken extends BaseMiddleware
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  \Closure $next
   * @return mixed
   */
  public function handle($request, \Closure $next)
  {
    if (!$token = $this->auth->setRequest($request)->getToken()) {
      return $this->respond('tymon.jwt.absent', 'token_not_provided', 400);
    }
    try {
      if (!$user = $user = $this->auth->authenticate($token)) {
        return $this->respond('tymon.jwt.invalid', 'token_invalid', 403);
      }
    } catch (TokenExpiredException $e) {

      return $this->respond('tymon.jwt.expired', 'token_expired', 403);

    } catch (\Exception $e) {

      return $this->respond('tymon.jwt.invalid', 'token_invalid', 403);

    }


    return $next($request);
  }
}
