const book = (state = {}, action) => {
  switch (action.type) {
    case 'FETCH_BOOK_SUCCESS':
      return action.response.book;
    default:
      return state
  }
};

export default book;