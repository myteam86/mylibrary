import React from 'react'
import {Pagination} from 'react-bootstrap'
import s from './Pagination.scss'

const PaginationAdvanced = React.createClass({
    getInitialState() {
        return {
            activePage: this.props.metadata.page
                ? this.props.metadata.page
                : 1,
            pages: Math.ceil(this.props.metadata.total / this.props.metadata.limit),
            maxButtons: 3
        };
    },
    componentWillReceiveProps (newProps){
        this.setState({
            activePage: parseInt(newProps.metadata.page),
            pages: Math.ceil(parseInt(newProps.metadata.total) / parseInt(newProps.metadata.limit))
        })
    },
    handleSelect(event, selectedEvent) {
        this.setState({
            activePage: selectedEvent.eventKey
        });
        if (this.props.handlePageChange != undefined) {
            this.props.handlePageChange(selectedEvent.eventKey)
        }

    },

    render() {
        return (
            <div className={s.cont}>
                <Pagination
                    className='pull-right'
                    prev
                    next
                    first
                    last
                    ellipsis
                    boundaryLinks
                    items={this.state.pages}
                    maxButtons={this.state.maxButtons}
                    activePage={this.state.activePage}
                    onSelect={this.handleSelect}/>
            </div>

        );
    }
});

export default PaginationAdvanced