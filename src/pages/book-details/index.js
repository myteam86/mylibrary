import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import Layout from '../../components/Layout';
import s from './styles.scss';
import {Row, Col, Grid, Media, Label} from 'react-bootstrap'
import * as actions from '../../actions';


class BookDetails extends React.Component {
  static propTypes = {};

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    let {id, fetchBookDetails} = this.props;
    fetchBookDetails(id);
  }

  render() {
    let {book} = this.props;
    return (
      <Layout className={s.content}>
        <Grid>
          <Row>
            <Col md={8} mdOffset={2} xs={12} xsOffset={0}>
              {!book.title
                ?
                <p className="text-center">Loading...</p>
                :
                <Media>
                  <h3>{book.title}</h3>
                  <Media.Left align="top">
                    <img className={s.cover}
                         src="https://cdn.pastemagazine.com/www/system/images/photo_albums/hobbit-book-covers/large/photo_5653_0.jpg?1384968217"
                         alt="..."/>
                  </Media.Left>
                  <Media.Body>
                    <Media.Heading>Book Description</Media.Heading>
                    <p>{book.summary}</p>
                    <h4>Book Details</h4>
                    <p>Publisher: {book.publisher}</p>
                    <p>By: {book.author}</p>
                    <p>Year: {book.year}</p>
                    <p>Bookshelf: {book.location.name}</p>
                    <p>
                      {
                        book.quantity > 0
                          ? <Label bsStyle="success">Available</Label>
                          : <Label bsStyle="danger">Not Available</Label>
                      }
                    </p>
                  </Media.Body>
                </Media>
              }
            </Col>
          </Row>
        </Grid>
      </Layout>
    );
  }
}

function mapStateToProps(state, ownProp) {
  let {book} = state;
  let id = ownProp.params.id;
  return {book, id}
}

export default connect(mapStateToProps, actions)(BookDetails);